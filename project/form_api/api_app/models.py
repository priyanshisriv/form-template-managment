from ast import For
from curses.ascii import US
import re
from statistics import mode
from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Base(models.Model):
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    

class FormTemplate(Base):
    name = models.CharField(max_length=500, null=True, blank=True)
    file = models.FileField(upload_to="field_records",max_length=250, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True )
    filled_by = models.ManyToManyField(User, related_name='form_filled_by')

    def __str__(self):
            return self.name

class FormField(models.Model):
    TEXT = "text"
    SINGLE_SELECT = "single_select"
    NUMBER = "number"
    DATE = "date"

    FIELD_CHOICES = [
        (TEXT, "text"),
        (SINGLE_SELECT, "single_select"),
        (NUMBER, "number"),
        (DATE, "date"),
    ]

    field_name = models.CharField(max_length=250, null=True, blank=True)
    field_type = models.CharField(max_length=100,choices=FIELD_CHOICES, null=True, blank=True) 
    mandatory = models.BooleanField(default=False)
    form_template = models.ForeignKey(FormTemplate, on_delete=models.CASCADE, null=True, blank=True)

    @property
    def template_name(self):
        return self.form_template.name
        
class FieldOptions(models.Model):
    select_field = models.ForeignKey(FormField, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
            return self.name

    class Meta:
        unique_together = ('select_field', 'name',)