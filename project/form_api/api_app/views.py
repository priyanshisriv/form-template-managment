
from rest_framework.response import Response
from rest_framework import status,permissions
from rest_framework.views import APIView
from .serializers import FormTemplateSerializer, FormFieldSerializer, FieldOptionSerializer
from .models import FormTemplate, FormField, FieldOptions
import pandas as pd
from rest_framework import generics, permissions
from django.http import Http404


class FormTemplateViewSet(generics.CreateAPIView):
    '''
    api to create form template
    '''
    serializer_class = FormTemplateSerializer
    permissions_classes = [permissions.IsAuthenticated]


    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        file = serializer.validated_data['file']
        form_name = serializer.validated_data['name']
     
        tmp_data=pd.read_csv(file, on_bad_lines='skip',encoding="unicode_escape",lineterminator='\n')
        row_iter = tmp_data.iterrows()
        template = FormTemplate(name = form_name, file = file, created_by = request.user)
        template.save()
        for index,row in row_iter:
            b = FormField(
                field_name = row['field_name'], 
                field_type = row['field_type'],
                mandatory = row['mandatory'],
               form_template = template)
            b.save()
            if b.field_type == 'single_select':
                arr = str(row['options']).split(",")
                for i in arr:
                    obj = FieldOptions(
                    select_field = b,
                    name =  i ,
                    )
                    obj.save()
        
        return Response({"status": "success","message":"Form template created"},
                        status.HTTP_201_CREATED)


class FormTemplateListSet(generics.ListAPIView):
    '''
    api to list form templates

    '''    
    serializer_class = FormFieldSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        query = FormField.objects.filter(form_template__created_by = self.request.user)
        return query

class FieldOptionsList(generics.ListAPIView):
    '''
    api to list form single select field options
    
    '''        
    serializer_class = FieldOptionSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        query = FieldOptions.objects.filter(select_field__form_template__created_by = self.request.user)
        return query    


class FormTemplateDetail(APIView):
    '''
    api to retrive, update, delete form template
    
    '''        

    def get_object(self, pk):
        try:
            return FormTemplate.objects.get(pk=pk)
        except FormTemplate.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)
        serializer = FormTemplateSerializer(obj)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FormTemplateSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        obj = self.get_object(pk)
        obj.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class FieldOptionDetail(APIView):
    '''
    api to retrive, update, delete form template fields
    
    '''        
    def get_object(self, pk):
        try:
            return FormField.objects.get(pk=pk)
        except FormTemplate.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)
        serializer = FormFieldSerializer(obj)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FormFieldSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        obj = self.get_object(pk)
        obj.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
