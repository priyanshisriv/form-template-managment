

from rest_framework import serializers
from .models import FieldOptions, FormField, FormTemplate
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email',)

class FormTemplateSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length= 250)
    file = serializers.FileField()


    class Meta:
        model = FormTemplate
        fields = ("name","file")
        read_only_fields = ['created_by']


class FieldOptionSerializer(serializers.ModelSerializer):
    select_field = serializers.ReadOnlyField(source='select_field.field_name')

    class Meta:
        model = FieldOptions
        fields = ("id","select_field","name")

class FormFieldSerializer(serializers.ModelSerializer):
    form_template = serializers.ReadOnlyField(source='form_template.name')
    # options = FieldOptionSerializer(read_only=True)
    class Meta:
        model = FormField
        fields = ("id","field_name","field_type","mandatory","form_template",)

  
  