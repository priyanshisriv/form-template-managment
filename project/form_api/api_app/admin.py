from django.contrib import admin
from .models import FieldOptions, FormTemplate, FormField
# Register your models here.
admin.site.register(FieldOptions)
admin.site.register(FormTemplate)
admin.site.register(FormField)
