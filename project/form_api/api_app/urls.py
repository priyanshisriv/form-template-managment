from django.urls import path,include
from .views import FormTemplateViewSet, FormTemplateListSet, FieldOptionsList, FormTemplateDetail, FieldOptionDetail
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
    path('token/refresh/',jwt_views.TokenRefreshView.as_view(),name ='token_refresh'),
    path('token/',jwt_views.TokenObtainPairView.as_view(),name ='token_obtain_pair'),
    path('add-template/', FormTemplateViewSet.as_view(),name = "create-template"),
    path('list-template/', FormTemplateListSet.as_view(),name = "list-template"),
    path('list-options/', FieldOptionsList.as_view(),name = "list-options"),
    path('forms/<int:pk>/', FormTemplateDetail.as_view()),
    path('fields/<int:pk>/', FieldOptionDetail.as_view()),
   
]