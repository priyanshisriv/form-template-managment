# Form Template Management


 ------------------------------------
## Getting started

- Create python virtual environment
- activate environment
  - source env/bin/activate(linux)
  - .env/Scripts/activate(windows)
- run pip install -r requirements.txt
- python manage.py migrate
- run python manage.py runserver


 ------------------------------------
#To access django admin 

- http://127.0.0.1:8000/admin/


 ------------------------------------
## API Endpoints(using postman)

#Get authentication token for logged-in user

- http://127.0.0.1:8000/api/token/ (POST)

body- username,password


 ------------------------------------
#To create form template(token required)

- http://127.0.0.1:8000/api/add-template/ (POST)

body- name, file(upload)
  
 ------------------------------------
#To list form templates for logged-in user(token required)

 - http://127.0.0.1:8000/api/list-template/ (GET)
 
 
 ------------------------------------
#To list options for single select field(token required)

- http://127.0.0.1:8000/api/list-options/ (GET)

-------------------------------------
#To get, update, delete form template(token required)

- http://127.0.0.1:8000/api/forms/<int:pk>/ ((PUT,DELETE))

body- name, file

--------------------------------------
#To get, update, delete form template fields(token required)

- http://127.0.0.1:8000/api/fields/<int:pk>/ (PUT,DELETE)

body- field_name, field_type, mandatory

 ------------------------------------
